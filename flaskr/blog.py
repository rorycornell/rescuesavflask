from flask import (
    Blueprint, flash, g, redirect, Response, render_template, request, url_for, jsonify
)
from werkzeug.exceptions import abort
import json
from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint('blog', __name__)


@bp.route('/', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':

        body = request.form['body']
        long = request.form['long']
        lat = request.form['lat']
        error = None



        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'INSERT INTO post (body, author_id, long, lat)'
                ' VALUES (?, ?, ?, ?)',
                (body, g.user['id'], long, lat)
            )
            db.commit()
            return redirect(url_for('blog.create'))

    else:
        db = get_db()
        posts = db.execute(
            'SELECT p.id, title, body, created, author_id, address, username, long, lat'
            ' FROM post p JOIN user u ON p.author_id = u.id'
            ' ORDER BY created DESC'
        ).fetchall()



    return render_template('blog/create.html', posts=posts)

def get_post(id, check_author=True):
    post = get_db().execute(
        'SELECT p.id, title, long, lat, body, created, address, author_id, username'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if post is None:
        abort(404, "Post id {0} doesn't exist.".format(id))

    if check_author and post['author_id'] != g.user['id']:
        abort(403)

    return post

@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    post = get_post(id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            db.execute(
                'UPDATE post SET title = ?, body = ?'
                ' WHERE id = ?',
                (title, body, id)
            )
            db.commit()
            return redirect(url_for('blog.index'))

    return render_template('blog/update.html', post=post)

@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    get_post(id)
    db = get_db()
    db.execute('DELETE FROM post WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('blog.index'))

@bp.route('/map')
def map():
    db = get_db()
    posts = db.execute(
        'SELECT p.id, title, body, created, author_id, username, long, lat'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' ORDER BY created DESC'
    ).fetchall()

    return render_template('blog/map.html', posts=posts)

@bp.route('/json')
def list():
    db = get_db()
    posts = db.execute(
        'SELECT p.id, title, body, created, author_id, username, address, long, lat'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' ORDER BY created DESC'
    ).fetchall()

    result=[]
    for post in posts:
        result.append({
        'body':post[2],
        'address':post[6],
        'longitude':post[7],
        'latitude':post[8],
        'time':post[3],
        'name':post[5],
        'user_id':post[4]
        })
    return jsonify(result)
